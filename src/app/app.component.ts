import { Component, ViewChild } from "@angular/core";
import { Events, NavController, Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Router } from "@angular/router";
import { AuthenticationService } from "./core/service/auth.service";
import { StorageClass } from "./core/class/storage.class";
import { AlertClass } from "./core/class/alert.class";
import { QuizService } from "./core/service/quiz.class";
import { Network } from "@ionic-native/network/ngx";
import { HttpClient } from "@angular/common/http";
@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"]
})
export class AppComponent {
  get logged() {
    return this.auth.currentUser();
  }

  // @ts-ignore
  @ViewChild("loader") loader;
  status = "CARREGANDO CAMPANHA";

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public auth: AuthenticationService,
    public router: Router,
    public events: Events,
    public nav: NavController,
    public storage: StorageClass,
    public _alert: AlertClass,
    public network: Network,
    public http: HttpClient
  ) {
    // setInterval(() => {
    //   this.http
    //     .get("https://sgi-api-sbd.herokuapp.com/api")
    //     .toPromise()
    //     .then(data => {
    //       this.storage.deleteItem("offline");
    //     })
    //     .catch(err => {
    //       this.storage.setItem("offline", "true");
    //     });
    // }, 2000);
    this.events.subscribe("offline", async () => {
      await this._alert
        .setHeader("SEM CONEXÃO!")
        .setMessage(
          `O seu dispositivo está sem conexão com a internet, para que você possa se registrar ou autenticar você precisa estar conectado, verifique sua conexão e tente novamente.`
        )
        .show();
    });

    this.network.onDisconnect().subscribe(() => {
      this.storage.setItem("offline", "true");
    });

    this.network.onConnect().subscribe(() => {
      this.storage.deleteItem("offline");
    });

    this.initializeApp();
    if (this.auth.currentUser()) {
      this.nav.navigateRoot(["/tabs"]);
    } else {
      this.storage.clear();
      this.nav.navigateRoot(["/"]);
    }
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (!this.storage.getItem("campaing")) {
        await this.auth
          .campaign()
          .then(campaing => {
            this.storage.setItem("campaing", campaing.data, true);
            this.events.publish("campaign_loaded");
            this.setCampaing(campaing.data);
            this.loader.nativeElement.classList.add("loaded");
            setTimeout(() => {
              this.loader.nativeElement.remove();
            }, 900);
          })
          .catch(error => {
            if (error === "Unknown Error") {
              this.storage.setItem("offline", true);
              this.status = `CARREGANDO CAMPANHA OFFLINE`;
              this.checkConnection();
            }
          });
      } else {
        let campaing = this.storage.getItem("campaing", true);
        this.setCampaing(campaing);
        this.loader.nativeElement.classList.add("loaded");
        setTimeout(() => {
          this.loader.nativeElement.remove();
        }, 900);
        this.events.publish("campaign_loaded");
      }
    });
  }

  checkConnection() {
    fetch("../assets/database/campaing.json").then(response => {
      response.json().then(campaing => {
        this.storage.setItem("campaing", campaing, true);
        this.events.publish("campaign_loaded");
        this.setCampaing(campaing);
        this.loader.nativeElement.classList.add("loaded");
        setTimeout(() => {
          this.loader.nativeElement.remove();
        }, 900);
      });
    });
  }

  setCampaing(campaing) {
    document.body.style.setProperty("--theme-primary", campaing.theme.primary);
    document.body.style.setProperty(
      "--ion-color-primary",
      campaing.theme.primary
    );
    document.body.style.setProperty(
      "--ion-color-primary-contrast",
      campaing.theme["primary-contrast"]
    );
    document.body.style.setProperty(
      "--ion-color-primary-shade",
      campaing.theme["secondary-contrast"]
    );
    document.body.style.setProperty(
      "--ion-color-secondary",
      campaing.theme.secondary
    );
    document.body.style.setProperty(
      "--ion-color-secondary-contrast",
      campaing.theme["secondary-contrast"]
    );
  }

  ionDidOpen() {}

  ionDidClose() {
    if (!Boolean(this.storage.getItem("offline"))) {
      this.events.publish("update_menu");
    }
  }
}
