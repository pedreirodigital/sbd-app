import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";

@Injectable({providedIn: 'root'})
export class StorageClass {

    constructor() {
    }

    private getKey(name) {
        return `${environment.app_key}_${name}`;
    }

    setItem(name, value, json = false) {
        if (json)
            value = JSON.stringify(value);
        localStorage.setItem(this.getKey(name), value);
    }

    getItem(name, json = false) {
        let data = localStorage.getItem(this.getKey(name));
        if (json)
            data = JSON.parse(data);
        return data;
    }

    deleteItem(name) {
        localStorage.removeItem(this.getKey(name));
    }

    clear() {
        localStorage.clear();
    }
}
