import {Injectable} from "@angular/core";
import {AlertController} from "@ionic/angular";

@Injectable({providedIn: 'root'})
export class AlertClass {

    header = null;
    subHeader = null;
    message = null;
    inputs = [];
    buttons = ['OK'];

    constructor(public alertController: AlertController) {
        this.reset();
    }

    setHeader(text) {
        this.header = text;
        return this;
    }

    setSubHeader(text) {
        this.subHeader = text;
        return this;
    }

    setMessage(text) {
        this.message = text;
        return this;
    }

    setButtons(array) {
        this.buttons = array;
        return this;
    }

    setInputs(array) {
        this.inputs = array;
        return this;
    }


    option() {
        let options: any = {};
        if (this.header !== null) {
            options.header = this.header;
        }
        if (this.subHeader !== null) {
            options.subHeader = this.subHeader;
        }
        if (this.message !== null) {
            options.message = this.message;
        }
        if (this.inputs !== null) {
            options.inputs = this.inputs;
        }
        if (this.buttons !== null) {
            options.buttons = this.buttons;
        }
        return options;
    }

    async show() {
        const alert = await this.alertController.create(this.option());
        await alert.present();
        this.reset();
    }

    reset() {
        this.header = null;
        this.subHeader = null;
        this.message = null;
        this.inputs = [];
        this.buttons = ['OK'];
    }
}
