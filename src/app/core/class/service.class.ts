import { Injectable, Input } from "@angular/core";
import { environment } from "../../../environments/environment";
import { Events, LoadingController } from "@ionic/angular";
import { OfflineClass } from "./offilne.class";
import { AlertClass } from "./alert.class";
import { StorageClass } from "./storage.class";

@Injectable({ providedIn: "root" })
export class ServiceClass extends OfflineClass {
  name = "";
  loading;
  result: any;
  storage = new StorageClass();

  constructor(
    public http,
    public loader: LoadingController,
    public events: Events,
    public alert: AlertClass
  ) {
    super(events);
  }

  /**
   *
   * @param id
   */
  async delete(id) {
    await this.http
      .delete(`${environment.host}/${this.name}/${id}`)
      .toPromise()
      .then(async data => this.success(data))
      .catch(async error => this.error(error));
    return this.result;
  }

  /**
   *
   * @param id
   */
  async getById(id) {
    await this.http
      .get(`${environment.host}/${this.name}/${id}`)
      .toPromise()
      .then(async data => this.success(data))
      .catch(async error => this.error(error));
    return this.result;
  }

  /**
   *
   * @param data
   */
  async save(data) {
    await this.http
      .post(`${environment.host}/${this.name}`, data)
      .toPromise()
      .then(async data => this.success(data))
      .catch(async error => this.error(error));
    return this.result;
  }

  async save2(data) {
    return this.http.post(`${environment.host}/${this.name}`, data).toPromise();
  }

  /**
   *
   * @param id
   * @param data
   */
  async update(id, data) {
    await this.http
      .put(`${environment.host}/${this.name}/${id}`, data)
      .toPromise()
      .then(async data => this.success(data))
      .catch(async error => this.error(error));
    return this.result;
  }

  /**
   *
   */
  async showLoading(text = "Aguarde....") {
    this.loading = await this.loader.create({
      message: text,
      id: "loader"
    });
    return await this.loading;
  }

  async success(data) {
    console.log(data);
    this.result = await data;
  }

  async error(error) {
    console.log(error);
    if (error === "Unknown Error") {
      this.events.publish("offline");
      return await this.serverLocal();
    } else {
      this.alert
        .setHeader("Oops!")
        .setMessage(error)
        .show();
    }
  }
}
