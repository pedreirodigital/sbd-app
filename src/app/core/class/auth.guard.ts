import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from "../service/auth.service";
import {StorageClass} from "./storage.class";


@Injectable({providedIn: 'root'})
/**
 *
 */
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        public storage: StorageClass
    ) {
    }

    /**
     *
     * @param route
     * @param state
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUser();


        if (currentUser) {
            return true;
        }

        this.router.navigate(['/home']);
        return false;
    }
}
