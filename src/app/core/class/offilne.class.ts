import {Injectable, Input} from "@angular/core";
import {environment} from "../../../environments/environment";
import {Events} from "@ionic/angular";

@Injectable({providedIn: 'root'})
export class OfflineClass {

    name;

    constructor(public events: Events) {
    }

    async serverLocal(login = false) {
        const data: any = localStorage.getItem(`${environment.app_key}_${this.name}`);
        if (data !== undefined || data !== null) {
            return JSON.parse(data);
        } else {
            return [];
        }
    }
}
