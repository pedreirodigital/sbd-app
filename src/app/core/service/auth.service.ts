import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { environment } from "../../../environments/environment";
import { Router } from "@angular/router";
import { StorageClass } from "../class/storage.class";
import { EventService } from "./event.service";
import { Events, NavController } from "@ionic/angular";

@Injectable({ providedIn: "root" })
export class AuthenticationService {
  auth;

  constructor(
    private http: HttpClient,
    public router: Router,
    public storage: StorageClass,
    public nav: NavController,
    public events: Events
  ) {}

  /**
   * Returns the current user
   */
  public currentUser() {
    if (this.storage.getItem("doctor", true)) {
      this.auth = this.storage.getItem("doctor", true);
    }

    return this.auth;
  }

  /**
   * Performs the auth
   * @param email email of user
   * @param password password of user
   */
  login(email: string, password: string) {
    return this.http
      .post<any>(`${environment.host}/doctor/login`, { email, password })
      .toPromise();
  }

  /**
   *
   */
  campaign() {
    return this.http
      .get<any>(`${environment.host}/campaign/${environment.id_app}`)
      .toPromise();
  }

  /**
   * Logout the user
   */
  logout() {
    this.auth = undefined;
    this.storage.deleteItem("doctor");
    this.storage.deleteItem("quiz");
    this.storage.deleteItem("hospital");
    this.nav.navigateRoot(["/"]);
  }
}
