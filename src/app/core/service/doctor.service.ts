import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ServiceClass } from "../class/service.class";
import { environment } from "../../../environments/environment";
import { Events, LoadingController } from "@ionic/angular";
import { AlertClass } from "../class/alert.class";

@Injectable({ providedIn: "root" })
export class DoctorService extends ServiceClass {
  name = "doctor";

  constructor(
    http: HttpClient,
    loader: LoadingController,
    events: Events,
    alert: AlertClass
  ) {
    super(http, loader, events, alert);
  }

  async recover(data) {
    await this.http
      .post(`${environment.host}/${this.name}/recover`, data)
      .toPromise()
      .then(async data => this.success(data))
      .catch(async error => this.error(error));
    return this.result;
  }

  async profile(id, data) {
    await this.http
      .put(`${environment.host}/${this.name}/profile/${id}`, data)
      .toPromise()
      .then(async data => this.success(data))
      .catch(async error => this.error(error));
    return this.result;
  }

  /**
   *
   * @param data
   * @param id
   */
  async putService(id, data) {
    await this.http
      .put(`${environment.host}/${this.name}/update/${id}`, data)
      .toPromise()
      .then(async data => this.success(data))
      .catch(async error => this.error(error));
    return this.result;
  }

  /**
   *
   * @param data
   * @param id
   * @param hospital
   */
  addService(data, id, hospital) {
    return this.http
      .put(`${environment.host}/${this.name}/service/${id}`, data)
      .toPromise();
  }

  async getData(id, return_promise = false) {
    let promise = this.http
      .get(`${environment.host}/${this.name}/data/${id}`)
      .toPromise();

    if (return_promise) return promise;
    else {
      await promise
        .then(async data => this.success(data))
        .catch(async error => this.error(error));
      return this.result;
    }
  }
}
