import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {ServiceClass} from "../class/service.class";
import {environment} from "../../../environments/environment";
import {Events, LoadingController} from "@ionic/angular";
import {AlertClass} from "../class/alert.class";

@Injectable({providedIn: 'root'})
export class HospitalService extends ServiceClass {

    name = 'hospital';

    constructor(http: HttpClient, loader: LoadingController, events: Events, alert: AlertClass) {
        super(http, loader, events, alert);
    }

    /**
     *
     */
    async getCode(code) {
        await this.http.get(`${environment.host}/${this.name}/code/${code}`).toPromise()
            .then(async data => this.success(data))
            .catch(async error => this.error(error));
        return this.result;
    }

    /**
     *
     */
    async residentIn(ids) {
        await this.http.post(`${environment.host}/${this.name}/resident`, ids).toPromise()
            .then(async data => this.success(data))
            .catch(async error => this.error(error));
        return this.result;
    }
}
