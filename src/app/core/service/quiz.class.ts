import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ServiceClass } from "../class/service.class";
import { environment } from "../../../environments/environment";
import { Events, LoadingController } from "@ionic/angular";
import { connectableObservableDescriptor } from "rxjs/internal/observable/ConnectableObservable";
import { AlertClass } from "../class/alert.class";

@Injectable({ providedIn: 'root' })
export class QuizService extends ServiceClass {

    name = 'quiz';

    constructor(http: HttpClient, loader: LoadingController, events: Events, alert: AlertClass) {
        super(http, loader, events, alert);
    }

    async questions(id) {
        await this.http.get(`${environment.host}/${this.name}/${id}`).toPromise()
            .then(async data => this.success(data))
            .catch(async error => this.error(error));
        return this.result;
    }

    async sync(data, id) {
        try {
            let request = await this.http.post(`${environment.host}/${this.name}/sync/${id}`, data).toPromise()
            return request
        } catch (e) {
            return undefined
        }
    }
}
