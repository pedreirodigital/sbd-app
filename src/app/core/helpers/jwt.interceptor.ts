import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthenticationService} from "../service/auth.service";
import {StorageClass} from "../class/storage.class";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService, public storage: StorageClass) {

    }

    /**
     *
     * @param request
     * @param next
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const currentUser = this.authenticationService.currentUser();

        if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: {
                    "Authorization": `Bearer ${currentUser.token}`,
                }
            });
        }

        return next.handle(request);
    }
}
