export interface DoctorModelInterface {
    hospital_id: string;
    photo: string;
    name: string;
    crm: number;
    phone: string;
    email: boolean;
    password: string;
    token?: string;
    admin?: boolean;
}
