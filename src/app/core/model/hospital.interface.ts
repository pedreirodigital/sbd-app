export interface HospitalInterface {
    _id: string;
    place: string;
    attach: string;
    name: string;
    district: string;
    city: string;
    state: string;
    phone: number;
    email: string;
    code: string;
    active: boolean;
}
