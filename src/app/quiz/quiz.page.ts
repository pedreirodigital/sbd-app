import {
  AfterViewInit,
  Component,
  Input,
  OnDestroy,
  OnInit
} from "@angular/core";
import { Events, ModalController } from "@ionic/angular";
import { isArray } from "rxjs/internal-compatibility";
import { AlertClass } from "../core/class/alert.class";
import { StorageClass } from "../core/class/storage.class";
import { QuizService } from "../core/service/quiz.class";
import { UUID } from "angular2-uuid";
import {cloneDeep} from 'lodash'
@Component({
  selector: "app-quiz",
  templateUrl: "./quiz.page.html",
  styleUrls: ["./quiz.page.scss"]
})
export class QuizPage implements OnInit, AfterViewInit {
  @Input("quizData") quizData;
  @Input("loading") loading;
  loader;
  data: any = {};
  steps: any = [];
  quiz: any = [];
  type = "form";
  campaing: any = {};

  user: any = {};
  current: any = {};
  loaded = false;

  constructor(
    private modalController: ModalController,
    public alert: AlertClass,
    public storage: StorageClass,
    public service: QuizService,
    public events: Events
  ) {
    this.campaing = this.storage.getItem("campaing", true);
    this.type = this.campaing.quiz.type;
    this.quiz = this.campaing.quiz.question;
    this.user = this.storage.getItem("doctor", true);
    this.current = this.user.services.find(s => s.current);
  }

  ngOnInit() {
    this.loadQuiz();
  }

  ngAfterViewInit() { }

  cleanData() {
    this.quiz.filter(f => {
      if (f.data) {
        f.data = f.data.map(m => {
          if (m.value["$numberInt"]) {
            m.value = m.value["$numberInt"];
          }
          return m;
        });
      }
      return f;
    });
  }

  async loadQuiz() {
    await this.cleanData();
    for (let item in this.quiz) {
      let obj = this.quiz[item];
      if (obj.type === "checkbox") {
        this.data[obj.name] = [];
      } else if (obj.type === "number") {
        this.data[obj.name] = 0;
      } else {
        this.data[obj.name] = null;
      }
    }

    if (this.quizData !== null && this.quizData !== undefined) {
      this.data = this.quizData;
    }

    this.loading.dismiss();
  }

  async closeModal() {
    await this.modalController.dismiss(null);
    this.quizData = null;
  }

  error = {};
  validate() {
    let valide = false;
    let text = `Preencha todas as perguntas do diagnóstico!`;
    let check = [];
    let i = 0;

    this.error = [];
    Object.keys(this.data).filter(v => {
      if (
        v !== "doctor_id" &&
        v !== "hospital_id" &&
        v !== "nome" &&
        v !== "_id" &&
        v !== "__v" &&
        v !== "deleted_at" &&
        v !== "deleted_at" &&
        v !== "synced_at" &&
        v !== "update_at" &&
        v !== "uuid" &&
        v !== "status" &&
        v !== "created_at" &&
        v !== "campaign_id"
      ) {
        if (this.data[v] === "" && !isArray(this.data[v])) {
          this.data[v] = null;
        }
        let questao: any = {};
        try{
          questao = this.quiz.find(q => q.name == v)
        }catch(e){
          console.log('DEU ERRO AQUI',v)
        }
        

        if (isArray(this.data[v])) {
          valide = this.data[v].length !== 0;
        } else if (questao.type === "number") {
          valide = this.data[v] !== 0;
        } else {
          valide = this.data[v] !== null;
        }
        if (!valide) this.error[questao.name] = questao.label;
        check.push(valide);
        i++;
      }
    });

    if (Object.keys(this.error).length > 0) {
      this.alert
        .setHeader(
          Object.keys(this.error).length === 1
            ? "Você deve preencher o seguinte campo!"
            : "Você deve preencher os seguintes campos!"
        )
        .setMessage(
          Object.keys(this.error)
            .map(key => `<p>${this.error[key]}</p>`)
            .join("")
        )
        .show();
      return false;
    }

    return valide;
  }

  getCheckboxValue(item, data, el) {
    if (!el.detail.checked) {
      this.data[item.name] = this.data[item.name].filter(v => v !== data.value);
    } else {
      this.data[item.name] = this.data[item.name].concat([data.value]);
    }
  }

  getRadioValue(name, el) {
    this.data[name] = parseInt(el.detail.value);
  }

  async save() {
    if (this.validate()) {
      this.loader = await this.service.showLoading(
        "AGUARDE SALVANDO DIAGNÓSTICO..."
      );
      await this.loader.present();
      if (this.data.idade !== null) {
        this.data.idade = parseInt(this.data.idade);
      }

      if (this.data._id === null || this.data._id === undefined) {
        this.data.created_at = new Date();
        this.data.update_at = null;
        this.data.deleted_at = null;
        this.data.synced_at = null;
        if (this.data.uuid === undefined) this.data.uuid = UUID.UUID();
      } else {
        this.data.update_at = new Date();
        this.data.synced_at = null;
      }

      this.data.doctor_id = this.user.id;
      this.data.hospital_id = this.storage.getItem("hospital");

      let campaign: any = this.storage.getItem("campaing", true)
      this.data.campaign_id = campaign._id;

      await this.modalController.dismiss(this.data);
      await this.loader.dismiss();
    }
  }

  checkedRadio(values, name) {
    if (this.quizData !== null && this.quizData !== undefined) {
      if (parseInt(values.value) === parseInt(this.quizData[name])) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkedCheck(values, name) {
    if (this.quizData !== null && this.quizData !== undefined) {
      return this.quizData[name].indexOf(parseInt(values.value)) !== -1;
    }
  }

  onKeyup(item) {
    if (item.type === "number") {
      if ([null, undefined, ""].indexOf(this.data[item.name]) === -1) {
        setTimeout(() => {
          let value = this.data[item.name].replace(/\D/g, "");
          value = parseInt(value);
          if (value > 99) value = "99";
          else String(value);
          this.data[item.name] = value;
        }, 250);
      }
    }
  }
}
