import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { DoctorService } from "../core/service/doctor.service";
import { HospitalService } from "../core/service/hospital.service";
import { AlertClass } from "../core/class/alert.class";
import { NavController, Events } from "@ionic/angular";
import { StorageClass } from "../core/class/storage.class";
import { AuthenticationService } from "../core/service/auth.service";
import { EventService } from "../core/service/event.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.page.html",
  styleUrls: ["./register.page.scss"]
})
export class RegisterPage implements OnInit {
  valide = false;
  saving = false;
  saving_code = false;
  campaing: any;
  loader;

  code: any = null;
  data: any = {
    name: "",
    crm: "",
    email: "",
    password: "",
    phone: ""
  };
  hospital: any = {};
  hasHospital = false;

  constructor(
    public router: Router,
    public doctorService: DoctorService,
    public hospitalService: HospitalService,
    public alert: AlertClass,
    public nav: NavController,
    public storage: StorageClass,
    public auth: AuthenticationService,
    public events: Events
  ) {
    this.campaing = this.storage.getItem("campaing", true);
  }

  ngOnInit() {}

  async getCode() {
    if (this.storage.getItem("offline") !== null) {
      this.events.publish("offline");
      return false;
    }

    if (this.code !== null) {
      this.loader = await this.hospitalService.showLoading(
        "CONSULTANDO CÓDIGO DO POSTO DE ATENDIMENTO..."
      );
      await this.loader.present();
      this.saving_code = true;
      this.hospital = await this.hospitalService.getCode(this.code);
      await this.loader.dismiss();
      console.log(this.hospital);
      if (this.hospital !== undefined) {
        this.hospital = this.hospital.data;
        this.valide = true;
        this.saving_code = false;
        this.hasHospital = true;
      } else {
        this.valide = false;
        this.saving_code = false;
        this.code = null;
      }
    } else {
      this.alert
        .setHeader("Código não informado!")
        .setMessage("Informe o código do posto de atemdimento.")
        .show();
    }
  }

  reset() {
    this.data = null;
    this.hospital = null;
  }

  async save() {
    if (
      this.data.name === "" &&
      this.data.crm === "" &&
      this.data.email === "" &&
      this.data.password === ""
    ) {
      this.alert
        .setHeader("Campos em branco!")
        .setMessage("Preencha os campos obrigatorios para continuar")
        .show();
    } else {
      if (!/\S+@\S+\.\S+/.test(this.data.email)) {
        this.alert
          .setHeader("Oops!")
          .setMessage("Você deve informar um e-mail válido!")
          .show();
        return false;
      }

      this.loader.present();
      this.data.hospital_id = this.hospital._id;
      this.saving = true;
      try {
        await this.doctorService.save2(this.data);
        this.login();
      } catch (e) {
        await this.alert
          .setHeader("Oops!")
          .setMessage(e)
          .show();
      }
      this.saving = false;
    }
  }

  async login() {
    await this.auth
      .login(this.data.email, this.data.password)
      .then(async result => {
        result.data.token = result.token;
        this.storage.setItem("doctor", result.data, true);
        this.reset();
        await this.nav.navigateRoot(["/tabs"]);
      })
      .catch(async error => {
        this.alert
          .setHeader("Oops!")
          .setMessage(error)
          .show();
      });
  }

  goto_login() {
    this.nav.navigateBack(["/login"]);
  }

  showLogo() {
    if (!this.hasHospital) {
      return this.campaing.logo_color;
    } else {
      return this.hospital.attach === ""
        ? this.campaing.logo_color
        : this.hospital.attach;
    }
  }
}
