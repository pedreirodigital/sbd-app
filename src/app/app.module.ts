import { enableProdMode, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";

import { JwtInterceptor } from "./core/helpers/jwt.interceptor";
import { ErrorInterceptor } from "./core/helpers/error.interceptor";
import { DashboardPageModule } from "./dashboard/dashboard.module";
import { MenuPage } from "./menu/menu.page";
import { NgxMaskIonicModule } from "ngx-mask-ionic";
import { Camera } from "@ionic-native/camera/ngx";
import { CameraProvider } from "./core/camera.provider";
import { File } from "@ionic-native/file/ngx";
import { Network } from "@ionic-native/network/ngx";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
enableProdMode();

@NgModule({
  declarations: [AppComponent, MenuPage],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    DashboardPageModule,
    NgxMaskIonicModule.forRoot()
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    File,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    CameraProvider,
    Network,
    InAppBrowser
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
