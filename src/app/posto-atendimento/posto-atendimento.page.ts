import { Component } from "@angular/core";
import { DoctorService } from "../core/service/doctor.service";
import { AuthenticationService } from "../core/service/auth.service";
import {
  ModalController,
  MenuController,
  Events,
  NavController
} from "@ionic/angular";
import { AlertClass } from "../core/class/alert.class";
import { StorageClass } from "../core/class/storage.class";
import { cloneDeep, orderBy } from "lodash";
@Component({
  selector: "app-posto-atendimento",
  templateUrl: "posto-atendimento.page.html",
  styleUrls: ["posto-atendimento.page.scss"]
})
export class PostoAtendimentoPage {
  loader: any;
  user: any = {};
  campaing: any = {};
  services: any = [];
  quiz: any = [];

  constructor(
    public service: DoctorService,
    public auth: AuthenticationService,
    public modalController: ModalController,
    public menu: MenuController,
    public alert: AlertClass,
    public storage: StorageClass,
    public events: Events,
    public navCtrl: NavController,
    public doctorSvc: DoctorService
  ) {
    this.events.subscribe("update_menu", () => {
      this.loadData();
    });
  }

  ionViewDidEnter() {
    this.loadData();
  }

  current = null;
  async loadData() {
    this.loader = await this.service.showLoading(
      "CARREGANDO POSTOS DE ATENDIMENTO AGUARDE..."
    );
    this.loader.present();
    this.user = this.storage.getItem("doctor", true);
    try {
      let response = await this.doctorSvc.getData(this.user.id, true);
      this.storage.setItem("doctor", response.data, true);
      this.user = response.data;
      this.events.publish("updateDashboard");
    } catch (e) {
      console.error(e)
    }
    this.services = this.user.services;
    this.ordenarServices();

    this.current = this.storage.getItem("hospital");
    if (this.current === null) this.current = this.services[0].id;

    this.loader.dismiss();
  }

  ordenarServices() {
    this.services = orderBy(cloneDeep(this.services), ["name"], ["asc"]);
  }

  async change(item) {
    this.storage.setItem("hospital", item.id);
    this.current = item.id;
    this.events.publish("updateDashboard");
    this.ordenarServices();
  }

  addService() {
    if (this.storage.getItem("offline") !== null) {
      this.events.publish("offline");
      return false;
    }
    this.alert
      .setHeader("")
      .setMessage(
        "Para atribuir um novo posto de atendimento ao seu perfil informe o código do mesmo abaixo:"
      )
      .setInputs([
        {
          name: "code",
          type: "text",
          placeholder: "CÓDIGO"
        }
      ])
      .setButtons([
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Adicionar",
          handler: async result => {
            if (result.code !== "" && result.code !== undefined) {
              let loader: any = await this.service.showLoading(
                "AGUARDE SALVANDO POSTO DE ATENDIMENTO..."
              );
              loader.present();
              if (Boolean(this.storage.getItem("offline"))) {
                this.events.publish("offline");
              } else {
                await this.service
                  .addService(result, this.user.id, this.current.id)
                  .then(success => {
                    this.services.push(success.data[0]);
                    this.user.services = this.services;
                    this.storage.setItem("doctor", this.user, true);
                    this.alert.setMessage(success.message).show();
                  })
                  .catch(error => {
                    if (error === "Unknown Error") {
                      this.events.publish("offline");
                      return false;
                    }
                    this.alert.setMessage(error).show();
                  });
              }
              loader.dismiss();
            } else {
              this.alert
                .setMessage("Informe o código do posto de atendimento")
                .show();
            }
          }
        }
      ])
      .show();
  }
}
