import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { DoctorService as Service } from "../core/service/doctor.service";
import { AlertClass } from "../core/class/alert.class";
import { AuthenticationService } from "../core/service/auth.service";
import { EventService } from "../core/service/event.service";
import { Events, NavController, BooleanValueAccessor } from "@ionic/angular";
import { StorageClass } from "../core/class/storage.class";
import { environment } from "../../environments/environment";
import { QuizService } from "../core/service/quiz.class";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  recover = false;
  sending = false;
  data = { email: null, password: null };
  campaing: any;

  constructor(
    public router: Router,
    public service: Service,
    public alert: AlertClass,
    public auth: AuthenticationService,
    public events: Events,
    public nav: NavController,
    public storage: StorageClass,
    public quizService: QuizService
  ) {
    this.campaing = this.storage.getItem("campaing", true);
  }

  ngOnInit() {}

  back() {
    this.router.navigate(["/"]);
  }

  async login() {
    if (this.storage.getItem("offline") !== null) {
      this.events.publish("offline");
      return false;
    }

    let loader: any = await this.service.showLoading();
    if (this.recover) {
      this.sending = true;
      await loader.present();
      await this.service
        .recover({ email: this.data.email, campaing: environment.id_app })
        .then(async result => {
          this.alert
            .setHeader("E-mail enviado com sucesso!")
            .setMessage(result.message)
            .show();
          this.sending = false;
          this.recover = false;
          await loader.dismiss();
        })
        .catch(async error => {
          this.alert
            .setHeader("Oops!")
            .setMessage(error)
            .show();
          this.sending = false;
          await loader.dismiss();
        });
    } else {
      if (this.data.email === null && this.data.password === null) {
        this.alert
          .setHeader("Campos em branco!")
          .setMessage("Preencha os campos E-mail e Senha para continuar!")
          .show();
      } else {
        if (!/\S+@\S+\.\S+/.test(this.data.email)) {
          this.alert
            .setHeader("Oops!")
            .setMessage("Você deve informar um e-mail válido!")
            .show();
          return false;
        }

        await loader.present();
        this.sending = true;
        await this.auth
          .login(this.data.email, this.data.password)
          .then(async result => {
            result.data.token = result.token;
            this.storage.setItem("doctor", result.data, true);
            this.sending = false;
            await loader.dismiss();

            let diagnosticos = await this.quizService.questions(result.data.id);
            this.storage.setItem("quiz", diagnosticos, true);
            await this.nav.navigateRoot(["/tabs"]);
          })
          .catch(async error => {
            if (error === "Unknown Error") {
              this.events.publish("offline");
              await loader.dismiss();
              this.sending = false;
              return false;
            }
            this.alert
              .setHeader("Oops!")
              .setMessage(error)
              .show();
            this.sending = false;
            await loader.dismiss();
          });
      }
    }
  }

  reset_password() {
    this.recover = true;
  }

  goto_login() {
    this.data.email = null;
    this.recover = false;
  }

  reset() {
    this.recover = false;
    this.sending = false;
    this.data = { email: null, password: null };
  }

  goto_register() {
    this.nav.navigateForward("/register");
  }

  textButton() {
    if (this.recover) {
      if (this.sending) {
        return "AGUARDE...";
      } else {
        return "RECUPERAR SENHA";
      }
    } else {
      return "ENTRAR";
    }
  }
}
