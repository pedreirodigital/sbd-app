import { Component, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { DomSanitizer } from "@angular/platform-browser";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
@Component({
  selector: "app-faq",
  templateUrl: "./faq.page.html",
  styleUrls: ["./faq.page.scss"]
})
export class FaqPage {
  constructor(
    public modalCtrl: ModalController,
    public domSanitizer: DomSanitizer,
    public inAppBrowser: InAppBrowser
  ) {}

  opened = null;
  open(index) {
    if (this.opened === index) {
      this.opened = null;
    } else {
      this.opened = index;
    }
  }

  sanitize(html) {
    return this.domSanitizer.bypassSecurityTrustHtml(html);
  }

  questions = [
    {
      index: 1,
      title: "Posso usar o aplicativo estando off-line?",
      content: `
      Sim. Após instalar o aplicativo da loja e fazer o primeiro acesso, você poderá usá-lo e preencher os diagnósticos mesmo estando sem conexão com internet.
      <br><br>No entanto, para sincronizar os dados com o servidor, será preciso acessar mais uma vez o aplicativo estando conectado a internet. 
      `
    },
    {
      index: 2,
      title:
        "Tenho que fazer a autenticação (login e senha) sempre que for usar o aplicativo?",
      content: `
      Não. Somente será preciso fazer a primeira autenticação. Depois que estiver conectado no aplicativo, você poderá fechá-lo e até mesmo desligar o seu smartphone.
      <br><br>Sempre que o chamar novamente, o app já irá abrir com o seu usuário autenticado. 
      `
    },
    {
      index: 3,
      title: "Posso me cadastrar em mais de um posto de atendimento?",
      content: `
      Sim. Sempre no primeiro acesso será necessário informar o código do posto de atendimento. Para ser vinculado a outro posto, basta clicar no botão <ion-icon name="ios-home"></ion-icon> <b>Postos de Atendimento</b> e em seguida no botão <ion-icon name="add"></ion-icon> localizado no canto inferior direito. Ali você deverá informar o código do novo posto que irá realizar os atendimentos.
      <br><br>Para vincular a um novo posto é necessário estar conectado na internet. 
      `
    },
    {
      index: 4,
      title: "Como faço para entrar com outro usuário?",
      content: `
      Para entrar no aplicativo com outro usuário, é necessário desconectar do seu usuário atual. Para isso basta clicar no botão <ion-icon name="ios-contact"></ion-icon> <b>Meu Perfil</b> e clicar na seta de saída localizada no canto superior direito. 
      <br><br>Caso esteja off-line, tenha diagnósticos não sincronizados e desejar desconectar, o aplicativo não irá permitir.`
    },
    {
      index: 5,
      title: "Como sei que o diagnóstico foi sincronizado no servidor?",
      content: `
      A sincronização é feita de forma automática e transparente. 
      <br><br>Para conferir se um diagnóstico já foi publicado, basta checar se ao lado de cada diagnóstico está exibindo o seguinte ícone <ion-icon src="assets/check-double.svg"></ion-icon>.
      <br><br>Dica: Sempre acompanhe o status do seu diagnóstico através das setas. O conceito usado é o mesmo do WhatsApp, ou seja, uma vez que está sendo exibido <ion-icon src="assets/check-double.svg"></ion-icon> significa que o seu diagnóstico foi sincronizado. 
      <br><br>O mais importante é checar no final de seus atendimentos se todos os diagnósticos foram sincronizados.`
    },
    {
      index: 6,
      title: "Posso alterar e/ou excluir um diagnóstico realizado?",
      content: `
      Sim. Todos os diagnósticos ficam disponíveis para alteração e/ou exclusão por até 30 minutos. Ao lado dos diagnósticos contém os botões <ion-icon name="create"></ion-icon> e <ion-icon name="trash"></ion-icon> indicados para tal ação. 
      <br><br>Após os 30 minutos do cadastro do mesmo, os botões somem e você não poderá mais realizar alteração nem exclusão.`
    },
    {
      index: 7,
      title: "Todos as perguntas dos diagnósticos devem ser respondidas? ",
      content: `
      Sim, exceto o Nome Completo. Somente deve ser preenchido em caso de suspeita de CA. 
      <br><br>Todos os demais campos são obrigatórios. O aplicativo irá orientar e sinalizar quais campos estão pendentes de preenchimento. 
      <br><br>Algumas perguntas aceitam mais de uma resposta.`
    }
  ];

  abrirManual() {
    this.inAppBrowser.create(
      "https://www.itarget.com.br/downloads/instrucoescapeleSBD.pdf"
    );
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }
}
