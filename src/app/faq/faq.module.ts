import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';
import {NgxMaskIonicModule} from "ngx-mask-ionic";
import { FaqPage } from './faq.page';



@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NgxMaskIonicModule.forRoot()
    ],
    declarations: [FaqPage],
    exports: [],
    entryComponents: [FaqPage]
})
export class FaqPageModule {
}
