import { AfterViewInit, Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthenticationService } from "../core/service/auth.service";
import { Events, NavController } from "@ionic/angular";
import { StorageClass } from "../core/class/storage.class";
import { AlertClass } from "../core/class/alert.class";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage implements OnInit, AfterViewInit {
  campaing: any = {};

  constructor(
    public router: Router,
    public auth: AuthenticationService,
    public nav: NavController,
    public storage: StorageClass,
    public events: Events,
    public alert: AlertClass
  ) {
    let campaign = this.storage.getItem("campaing", true);
    if ([null, undefined].indexOf(campaign) === -1) {
      this.campaing = campaign;
    } else {
      this.events.subscribe("campaign_loaded", () => {
        this.campaing = this.storage.getItem("campaing", true);
      });
    }
  }

  async register() {
    if (this.storage.getItem("offline") !== null) {
      this.events.publish("offline");
      return;
    }
    this.nav.navigateForward("/register");
  }

  async login() {
    if (this.storage.getItem("offline") !== null) {
      this.events.publish("offline");
      return;
    }
    this.nav.navigateForward("/login");
  }

  ngOnInit() {}

  ngAfterViewInit() {}
}
