import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "../core/service/auth.service";
import { MenuController, ModalController, NavController } from "@ionic/angular";
import { ModalPage } from "../modal/modal.page";
import { AlertClass } from "../core/class/alert.class";
import { DoctorService } from "../core/service/doctor.service";
import { StorageClass } from "../core/class/storage.class";
import { Events } from "@ionic/angular";
import { EventService } from "../core/service/event.service";
import set = Reflect.set;
import {cloneDeep} from 'lodash'
@Component({
    selector: "app-menu",
    templateUrl: "./menu.page.html",
    styleUrls: ["./menu.page.scss"]
})
export class MenuPage implements OnInit {
    loader: any;
    user: any = {};
    campaing: any = {};
    services: any = null;
    current: any = {};
    quiz: any = [];

    constructor(
        public service: DoctorService,
        public auth: AuthenticationService,
        public modalController: ModalController,
        public menu: MenuController,
        public alert: AlertClass,
        public storage: StorageClass,
        public events: Events,
        public navCtrl: NavController
    ) {
        this.events.subscribe('update_menu', () => {
            this.loadData();
        });
    }

    ngOnInit() {
        this.loadData();
    }

    get current_hospital() {
        if (this.services === null) return ''
        let hospital = this.services.find(s => s.id == this.storage.getItem('hospital'))
        if (hospital === undefined) return ''
        return hospital.name
    }

    goToProfile() {
        this.navCtrl.navigateForward('/perfil')
        this.menu.toggle()
    }

    async loadData() {
        try {
            this.user = await this.storage.getItem("doctor", true);

            console.log(cloneDeep(this.user))

            try {
                let dataUser: any = await this.service.getData(this.user.id);
                this.storage.setItem("doctor", dataUser.data, true);
                this.user = this.storage.getItem("doctor", true);
                this.storage.setItem('quiz', dataUser.quiz, true);
            } catch (e) {
                this.user = this.storage.getItem("doctor", true);

            }

            this.services = this.user.services;
            this.campaing = this.storage.getItem("campaing", true);
            this.quiz = this.storage.getItem("quiz", true);
            this.current = this.user.services.find(s => s.current);
        } catch (e) {

        }
    }

    async openModal() {
        const modal = await this.modalController.create({
            component: ModalPage,
            componentProps: {
                user: this.user
            }
        });

        modal.onWillDismiss().then(result => {
            this.user = result.data;
            this.events.publish('updata_user', this.user);
            this.storage.setItem("doctor", this.user, true);
        });

        return await modal.present();
    }

    showLogo() {
        if (this.campaing.logo_invert) {
            if (this.current.logo !== '') {
                return this.current.logo;
            } else {
                return this.campaing.logo;
            }
        } else {
            if (this.current.logo !== '') {
                return this.current.logo;
            } else {
                return this.campaing.logo_color;
            }
        }
    }
}
