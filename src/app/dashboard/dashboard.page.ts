import { Component, OnInit } from "@angular/core";
import { HospitalService } from "../core/service/hospital.service";
import { AuthenticationService } from "../core/service/auth.service";
import {
  MenuController,
  ModalController,
  ToastController
} from "@ionic/angular";
import { StorageClass } from "../core/class/storage.class";
import { AlertClass } from "../core/class/alert.class";
import { Events } from "@ionic/angular";
import * as moment from "moment";
import { QuizService } from "../core/service/quiz.class";
import { QuizPage } from "../quiz/quiz.page";
import { environment } from "../../environments/environment";
import { uniq, cloneDeep } from "lodash";
import { FaqPage } from '../faq/faq.page';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.page.html",
  styleUrls: ["./dashboard.page.scss"]
})
export class DashboardPage implements OnInit {
  user: any = [];
  services: any = [];
  questionnaires: any = [];
  current: any = {};
  loader: any;
  timeAction = environment.timeAction;
  toast;
  hasSync = false;

  get offline() {
    return this.storage.getItem("offline") !== null;
  }

  constructor(
    public service: HospitalService,
    public quizService: QuizService,
    public auth: AuthenticationService,
    public storage: StorageClass,
    public menu: MenuController,
    public alert: AlertClass,
    public events: Events,
    public modalController: ModalController,
    public http: HttpClient
  ) {
    this.events.subscribe("updata_user", data => {
      this.user = this.auth.currentUser();
    });

    this.events.subscribe("sync", async () => {
      this.hasSync = true;
      this.sync();
    });

    this.events.subscribe("updateDashboard", obj => {
      this.loadData();
    });

    setInterval(() => {
      this.events.publish("sync");
    }, environment.syncTimer);
  }

  async ngOnInit() {
    await this.loadData();
    await this.sync();
  }

  diagnosticos: any = [];
  diagnosticos_por_posto: any = [];
  async loadData() {
    if (!this.hasSync) {
      this.loader = await this.service.showLoading(
        "CARREGANDO DIAGNÓTICOS AGUARDE..."
      );
      this.loader.present();
    }
    this.user = this.auth.currentUser();
    if (this.storage.getItem("hospital") === null) {
      this.storage.setItem("hospital", this.user.services[0].id);
    }
    this.current = this.user.services.find(
      f => f.id == this.storage.getItem("hospital")
    );

    this.storage.setItem("hospital", this.current.id);

    this.readQuiz();

    await this.loader.dismiss();
  }

  async openQuiz() {
    let loader: any = await this.service.showLoading(
      "CARREGANDO FORMULÁRIO DE DIAGNÓSTICO"
    );
    loader.present();
    const modal = await this.modalController.create({
      component: QuizPage,
      componentProps: {
        loading: loader
      }
    });

    modal.onWillDismiss().then(async result => {
      if (this.updateQuiz(result.data)) this.storeQuiz();
    });

    await modal.present();
  }

  async editQuiz(item) {
    let loader: any = await this.service.showLoading(
      "CARREGANDO FORMULÁRIO DE DIAGNÓSTICO"
    );
    loader.present();
    const modal = await this.modalController.create({
      component: QuizPage,
      componentProps: {
        quizData: Object.assign({}, item),
        loading: loader
      }
    });

    modal.onWillDismiss().then(result => {
      if (this.updateQuiz(result.data)) this.storeQuiz();
    });
    return await modal.present();
  }

  dateDiff(value) {
    let data1 = moment(new Date(), "DD/MM/YYYY hh:mm:ss");
    let data2 = moment(new Date(value), "DD/MM/YYYY hh:mm:ss");
    return data1.diff(data2, "minutes");
  }

  filtrarDiagnosticosPorPosto() {
    this.diagnosticos_por_posto = this.diagnosticos.filter(
      q => q.hospital_id === this.current.id
    );
  }

  deleteQuiz(item, el) {
    this.alert
      .setHeader("Deseja excluir?")
      .setMessage("Ao excluir este diagnóstico não será mais possivel recuperá-lo, deseja excluir assim mesmo?")
      .setButtons([
        {
          text: "Sim, desejo excluir!",
          handler: async () => {
            item.synced_at = null;
            item.deleted_at = new Date();
            this.updateQuiz(item);
            this.storeQuiz();
            this.alert
              .setHeader("Excluido com sucesso!")
              .setMessage("Diagnóstico excluido com sucesso")
              .show();
          }
        },
        {
          text: "Não",
          role: "cancel"
        }
      ])
      .show();
  }

  filter(event) {
    alert(event);
  }

  showHideButton(item) {
    return this.dateDiff(item.created_at) < this.timeAction;
  }

  readQuiz(return_array = false) {
    let quiz: any = this.storage.getItem("quiz", true);
    if (quiz === null) quiz = [];
    let quiz_ids = uniq(quiz.map(q => q.uuid));
    quiz = quiz_ids.map(uuid => quiz.find(q => q.uuid == uuid));
    if (return_array) return quiz;

    this.diagnosticos = quiz;
    this.filtrarDiagnosticosPorPosto();
  }

  updateQuiz(quiz) {
    if ([null, undefined].indexOf(quiz) !== -1) return false;

    this.diagnosticos = this.diagnosticos.filter(d => d.uuid != quiz.uuid)
    this.diagnosticos.push(quiz);

    return true;
  }

  async sync() {
    let quiz: any = Object.assign({}, cloneDeep(this.readQuiz(true)));
    let checkUpdate = Object.values(quiz).filter(
      (f: any) => f.synced_at === null
    );

    if (checkUpdate.length !== 0 && this.storage.getItem("offline") === null) {
      let newQUiz = await this.quizService.sync(checkUpdate, this.user.id);
      if (newQUiz !== undefined) {
        this.diagnosticos = newQUiz;
        this.filtrarDiagnosticosPorPosto();
        this.storeQuiz();
        this.hasSync = false;
      }
    }
  }

  storeQuiz() {
    if (this.diagnosticos !== undefined) {
      this.storage.setItem("quiz", this.diagnosticos, true);
      this.filtrarDiagnosticosPorPosto();
    }
  }

  async goToFaq() {
    let modal = await this.modalController.create(
      {
        component: FaqPage
      }
    )
    modal.present()
  }

  async reportErrorSync() {


    this.alert
      .setHeader("Deseja sincronizar?")
      .setMessage("Todos os seus diagnósticos serão enviados para nosso servidor. Faremos uma análise de seus dados para identificar se contém algo que ainda não foi sincronizado e o motivo. Enquanto não efetuarmos o processamento, seus diagnósticos não sincronizados permanecerão com apenas uma setinha. Faremos contato com orientações caso seja necessário.")
      .setButtons([
        {
          text: "Sim, desejo sincronizar!",
          handler: async () => {


            let doctor:any = this.storage.getItem('doctor', true)
            let data: any = await this.http.post(environment.host + '/quiz/report-error-sync', {
              name: doctor.name,
              email: doctor.email,
              id: doctor.id,
              diagnostics: this.storage.getItem('quiz', true)
            }).toPromise()

            this.alert
              .setHeader(data.title)
              .setMessage(data.message)
              .show();
          }
        },
        {
          text: "Não",
          role: "cancel"
        }
      ])
      .show();
  }
}
