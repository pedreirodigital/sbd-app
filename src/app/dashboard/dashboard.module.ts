import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { DashboardPage } from './dashboard.page';
import { ModalPage } from "../modal/modal.page";
import { QuizPage } from "../quiz/quiz.page";
import { NgxMaskIonicModule } from "ngx-mask-ionic";
import { FaqPageModule } from '../faq/faq.module';

const routes: Routes = [
    {
        path: '',
        component: DashboardPage,
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        FaqPageModule,
        RouterModule.forChild(routes),
        NgxMaskIonicModule.forRoot()
    ],
    declarations: [DashboardPage, QuizPage],
    exports: [],
    entryComponents: [QuizPage]
})
export class DashboardPageModule {
}
