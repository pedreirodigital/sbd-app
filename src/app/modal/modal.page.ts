import {
  AfterViewInit,
  Component,
  Input,
  OnDestroy,
  OnInit
} from "@angular/core";
import { Events, ModalController } from "@ionic/angular";
import { EventService } from "../core/service/event.service";
import { StorageClass } from "../core/class/storage.class";
import { DoctorService } from "../core/service/doctor.service";
import { AlertClass } from "../core/class/alert.class";
import { CameraProvider } from "../core/camera.provider";
import { AuthenticationService } from "../core/service/auth.service";

@Component({
  selector: "app-modal",
  templateUrl: "./modal.page.html",
  styleUrls: ["./modal.page.scss"]
})
export class ModalPage implements OnInit {
  total: any = {};
  disabled = false;
  loader;
  user: any = {
    admin: false
  };

  constructor(
    private modalController: ModalController,
    public serviceDoctor: DoctorService,
    public auth: AuthenticationService,
    public events: Events,
    public storage: StorageClass,
    public alert: AlertClass,
    public camera: CameraProvider
  ) {}

  total_diagnosticos = 0;
  async ionViewDidEnter() {
    this.editing = false;
    let quiz: any = this.storage.getItem("quiz", true);
    if (quiz === null) quiz = [];
    this.total_diagnosticos = quiz.length;

    this.user = await this.storage.getItem("doctor", true);
    this.total_postos = this.user.services.length;
  }

  editing = false;
  edit() {
    if (this.storage.getItem("offline") !== null) {
      this.events.publish("offline");
      return false;
    }
    this.editing = !this.editing;
  }

  total_postos = 0;

  async ngOnInit() {}

  logout() {
    let quiz: any = this.storage.getItem("quiz", true);
    if (quiz === null) quiz = [];
    let checkQuiz = Object.values(quiz).filter(
      (f: any) => f.synced_at === null
    );
    if (checkQuiz.length !== 0) {
      this.alert
        .setHeader("Oops!")
        .setMessage("Você possui dados a serem sincronizados.")
        .show();
      return false;
    }

    this.alert
      .setHeader("")
      .setMessage("Deseja realmente sair do aplicativo?")
      .setButtons([
        {
          text: "Sim, desejo!",
          handler: () => {
            this.auth.logout();
          }
        },
        {
          text: "Não",
          role: "cancel"
        }
      ])
      .show();
  }

  async closeModal() {
    await this.modalController.dismiss(this.user);
  }

  foto() {
    this.camera.takePicture(
      async base64 => {
        this.user.photo = base64;
        this.storage.setItem("doctor", this.user, true);
      },
      128,
      128
    );
  }

  async save() {
    if (Boolean(this.storage.getItem("offline"))) {
      this.events.publish("offline");
      return false;
    }

    this.loader = await this.serviceDoctor.showLoading("SALVANDO DADOS...");
    await this.loader.present();
    let getData = Object.assign({}, this.user);

    delete getData["id"];
    delete getData["admin"];
    delete getData["services"];
    delete getData["token"];

    if ([undefined, null].indexOf(this.storage.getItem("offline")) !== -1) {
      let result: any = await this.serviceDoctor.profile(this.user.id, getData);
      this.events.publish("update_menu");
      this.alert.setMessage(result.message).show();
      this.editing = false;
      await this.loader.dismiss();
    } else {
      this.storage.setItem("doctor", this.user, true);
      this.editing = false;
      await this.loader.dismiss();
    }
  }
}
