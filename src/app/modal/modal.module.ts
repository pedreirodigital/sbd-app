import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';
import {ModalPage} from "../modal/modal.page";
import {NgxMaskIonicModule} from "ngx-mask-ionic";

const routes: Routes = [
    {
        path: '',
        component: ModalPage,
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        NgxMaskIonicModule.forRoot()
    ],
    declarations: [ModalPage],
    exports: [],
    entryComponents: [ModalPage]
})
export class ModalPageModule {
}
