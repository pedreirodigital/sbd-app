keytool -genkey -v -keystore my-release-key.keystore -alias sbpt2019 -keyalg RSA -keysize 2048 -validity 10000

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore android-release-unsigned.apk sbpt2019

zipalign -v 4 android-release-unsigned.apk sbpt2019.apk