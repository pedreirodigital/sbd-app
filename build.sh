#/bin/bash

export PATH=$PATH:~/Programas/android-sdk/build-tools/29.0.2/zipalign

rm -rf platforms/android

if [ -z "$1" ]
  then
    echo "VOCÊ DEVE INFORMAR O NOME DO APP ./build.sh nome_do_app"
    exit 1
fi

APP=$1
CERT_PATH="./cert/$APP.keystore"
CERT_ALIAS=itarget
CERT_PASSWORD=123456

if [ -f $CERT_PATH ]
then
    echo "##################### CERTIFICADO $CERT_PATH ENCONTRADO"
else
    echo "##################### GERANDO O CERTIFICADO $CERT_PATH"
    keytool -genkey -v -keystore $CERT_PATH -alias $CERT_ALIAS -keyalg RSA -keysize 2048 -validity 100000
fi

echo "##################### GERANDO O APK"
ionic cordova build --release android

APK=platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk
echo "##################### ASSINANDO O APK"
echo $CERT_PASSWORD | jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $CERT_PATH $APK $CERT_ALIAS
rm -rf $APP.apk
echo "##################### COMPACTANDO O APK"
/Users/flaviosouza/Library/Android/sdk/build-tools/28.0.3/zipalign -v 4 $APK $APP.apk
# /home/fsouza/Programas/android-sdk/build-tools/29.0.2/zipalign -v 4 $APK $APP.apk